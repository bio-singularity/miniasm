# Miniasm singularity recipe

A Singularity recipe to create an image which wrap the minimap2+miniasm mapping and assembly tools.

We use Minimap 2 v2.12 and Miniasm v0.3 (latest stable release [Aug 2018]).

## Get it

### From the recipe

```bash
	sudo singularity build miniasm.simg Singularity
```

### Get the image from repositories

Will be available soon on singularity-hub. We are waiting for a compatibility with gitlab.

## Use it

### launch Minimap2

```bash
	singularity exec miniasm.simg minimap2 <arguments>
```

### launch Miniasm

```bash
	singularity exec miniasm.simg miniasm <arguments>
```

### Perform a long read assembly

```bash
	./miniasm.simg -fq <fastq/fasta reads> [-out_prefix <prefix> -t <threads>]
```

By default all the configurations are done for a packbio de novo read assembly.
If you want to use nanopore reads, please set the correct -x option for minimap2.  
The -out_prefix is used to create the outputs of minimap (.paf.gz) and miniasm (.gfa).
If this option is not set, the singularity will use the input filename (without the extention) as prefix.

## Examples

* Pacbio assembly from a fasta file called reads.fa on a 16 core computer.
The outputs will be put in the homedir and called "my_data":

```bash
	./miniasm.simg -t 16 -fq ./reads.fa -out_prefix ~/my_data
```

* Nanopore assembly from a fastq file called reads.fq.
./reads.paf.gz and ./reads.gfa will be created:

```bash
	./miniasm.simg -x ava-ont -fq ./reads.fq
```

## Links

* [Minimap/miniasm publication](https://academic.oup.com/bioinformatics/article/32/14/2103/1742895)
* [Minimap2](https://academic.oup.com/bioinformatics/advance-article/doi/10.1093/bioinformatics/bty191/4994778)
* [Minimap2 github](https://github.com/lh3/minimap2)
* [Miniasm github](https://github.com/lh3/miniasm)
* [Singularity](https://www.sylabs.io/)
