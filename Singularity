Bootstrap: docker
From: ubuntu:16.04


%labels
	maintainer Yoann Dufresne <yoann.dufresne@pasteur.fr>
	minimap2.version 2.12
	miniasm.version 0.3

%setup
	if [ -d ${SINGULARITY_ROOTFS}/scripts ]; then
		rm -r ${SINGULARITY_ROOTFS}/scripts
	fi
	mkdir ${SINGULARITY_ROOTFS}/scripts

%files
	arguments.py /scripts
	gfa2unitigs.py /scripts

%post
	##### System #####
	apt update -y
	apt upgrade -y
	apt install -y build-essential wget zlib1g-dev python3
	
	if [ -d /local_tmp ]; then
		rm -r /local_tmp
	fi
	mkdir /local_tmp

	##### Minimap2 #####
	wget -P /local_tmp/ https://github.com/lh3/minimap2/archive/v2.12.tar.gz
	cd /local_tmp
	tar xvzf /local_tmp/v2.12.tar.gz
	rm /local_tmp/v2.12.tar.gz
	if [ -d /minimap2 ]; then
		rm -r /minimap2
	fi
	mv /local_tmp/minimap2-2.12 /minimap2
	cd /minimap2
	make
	if [ ! -f /usr/local/bin/minimap2 ]; then
		ln -s /minimap2/minimap2 /usr/local/bin/minimap2
	fi


	##### Miniasm #####
	wget -P /local_tmp/ https://github.com/lh3/miniasm/archive/v0.3.tar.gz
	cd /local_tmp
	tar xvzf /local_tmp/v0.3.tar.gz
	rm /local_tmp/v0.3.tar.gz
	if [ -d /miniasm/ ]; then
		rm -r /miniasm/
	fi
	mv /local_tmp/miniasm-0.3/ /miniasm/
	cd /miniasm/
	make
	if [ ! -f /usr/local/bin/miniasm ]; then
		ln -s /miniasm/miniasm /usr/local/bin/miniasm
	fi



%runscript
	MINIMAP_ARGS=$(python3 /scripts/arguments.py minimap2 $@)
	MINIASM_ARGS=$(python3 /scripts/arguments.py miniasm $@)
	PREFIX=$(python3 /scripts/arguments.py out_prefix $@)

	minimap2 $MINIMAP_ARGS | gzip -1 > "$PREFIX.paf.gz"
	miniasm $MINIASM_ARGS > "$PREFIX.gfa"
	python3 /scripts/gfa2unitigs.py < "$PREFIX.gfa" > "$PREFIX.utg"
