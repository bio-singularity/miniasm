import sys

for line in sys.stdin:
    if line[0] == 'S':
        split = line.split('\t')
        print(">{}\n{}".format(split[1], split[2]))
