import sys


def print_minimap_args(args):
	# Define default options
	x = "ava-pb"
	t = 1
	fq = ""

	# Read mandatory arguments
	if not "-fq" in args:
		print("The input fastq file must be present with option -fq", file=sys.stderr)
		exit(1)

	fq = args["-fq"]

	# Modify default options with arguments
	if "-x" in args:
		x = args["-x"]
	if "-t" in args:
		t = int(args["-t"])

	# Print the command line parameters
	print("-x {} -t{} {} {}".format(x, t, fq, fq))


def print_miniasm_args(args):
	# Read mandatory arguments
	if not "-fq" in args:
		print("The input fastq file must be present with option -fq", file=sys.stderr)
		exit(1)

	fq = args["-fq"]
	out_prefix = fq[:fq.rfind(".")]

	# Read optional arguments
	if "-out_prefix" in args:
		out_prefix = args["-out_prefix"]

	print("-f {} {}.paf.gz".format(fq, out_prefix))


def print_prefix_args (args):
	if "-out_prefix" in args:
		print(args["-out_prefix"])
	else:
		fq = args["-fq"]
		print(fq[:fq.rfind(".")])


def parse(argv):
	args = {}
	current_arg = {"name":None, "value":None}

	# Parse all the words in the arguments
	for word in argv:
		if word[0] == '-':
			# Save previous argument
			if current_arg["name"] != None:
				args[current_arg["name"]] = current_arg["value"]

			# Create new argument
			current_arg = {"name":word, "value":None}
		else:
			if current_arg["value"] == None:
				current_arg["value"] = word
			elif isinstance(current_arg["value"], list):
				current_arg["value"].append(word)
			else:
				current_arg["value"] = [current_arg["value"], word]

	# Add the last argument
	if current_arg["name"] != None:
		args[current_arg["name"]] = current_arg["value"]

	return args


def read_arguments(argv):
	# If no parser specified return crash
	if len(argv) == 0:
		print("Impossible to parse arguments without a program specified", file=sys.stderr)
		exit(1)

	# Parse arguments
	args = parse(argv[1:])

	# Filter the arguments regarding the software
	if argv[0] == "minimap2":
		print_minimap_args(args)
	elif argv[0] == "miniasm":
		print_miniasm_args(args)
	elif(argv[0]) == "out_prefix":
		print_prefix_args(args)
	else:
		print("No program named {}".format(argv[0]), file=sys.stderr)
		exit(2)

	exit(0)


if __name__ == "__main__":
	read_arguments(sys.argv[1:])